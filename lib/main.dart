import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uniyalasbawarchiapp/HomePage.dart';
import 'package:uniyalasbawarchiapp/SplashScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
Widget build(BuildContext context) {
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  return new MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Flutter Demo',
    theme: new ThemeData(
        primarySwatch: Colors.blue,
        hintColor: Colors.white,
        inputDecorationTheme: new InputDecorationTheme(
            labelStyle: new TextStyle(color: Colors.white),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0)))),
    home: new SplashScreen(),
    routes: <String, WidgetBuilder>{
      '/HomePage': (BuildContext context) => new HomePage()
    },
  );
}
}