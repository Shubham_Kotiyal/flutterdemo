import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SplashScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => SplashPageState();

}

class SplashPageState extends State<SplashScreen> {

  @override
  Widget build(BuildContext context) {
    void navigationToNextPage() => Navigator.pushNamed(context, '/LoginScreen');

    startSplashScreenTimer() async {
      var _duration = new Duration(seconds: 5);
      return new Timer(_duration, navigationToNextPage);
    }

    @override
    void initState() {
      super.initState();
      startSplashScreenTimer();
    }

    @override
    Widget build(BuildContext context) {
      SystemChrome.setEnabledSystemUIOverlays([]);
      return Container(
          child: new Image.asset('assets/logo.png', fit: BoxFit.fill));
    }
  }

}